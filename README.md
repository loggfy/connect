## A Log Handler for Monolog and Laravel PHP Framework


## Installation

Add the following to your composer.json and run `composer update`

```json
{
    "require": {
        "loggfy/connect": "dev-master"
    }
}
```

Don't forget to dump composer autoload

```php
composer dump-autoload
```

Open your config/app.php add following line in the providers array

```php
Loggfy\Connect\LoggfyServiceProvider::class
```
 

Then in your bootstrap/app.php add / update your Monolog configuration.

```php
$app->configureMonologUsing(function ($monolog) {
    $monolog->pushHandler(new \Loggfy\Connect\Loggfy());
});
```


Run following command to publish migration and configuration


```php
 php artisan vendor:publish --provider="Loggfy\Connect\LoggfyServiceProvider"
```
 


Thats all now start to log the events.

Open config/loggfy.php file and update the settings.

### USAGE

Do not fotget to include Log to your class

```php
use Log;
```

And add log entry
```php
Log::info('user.register', ['message' => 'User Registration Controller', 'id' => 23, 'name' => 'John Doe', 'email' => 'john@example.com']);
```

Browse to [loggfy.com]http://panel.loggfy.com, login to your account and check your log entry exists there.

### Links
[loggfy.com](http://www.loggfy.com)
