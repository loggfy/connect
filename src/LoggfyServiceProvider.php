<?php
namespace Loggfy\Connect;

use Illuminate\Support\ServiceProvider;

class LoggfyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            realpath(__DIR__.'/config/loggfy.php') => config_path('loggfy.php'),
        ], 'loggfy');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom( __DIR__.'/config/loggfy.php', 'loggfy');
        $this->app->singleton('loggly', function ($app) {
            return new Loggfy;
        });
    }
}
