<?php
return [

    // enter your api key 
    'api_key'   => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImp0aSI6IjRmMWcyM2ExMmFhIn0.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLmNvbSIsImF1ZCI6Imh0dHA6XC9cL2V4YW1wbGUub3JnIiwianRpIjoiNGYxZzIzYTEyYWEiLCJpYXQiOjE1MTIyOTE2ODMsIm5iZiI6MTUxMjI5MTc0MywiY29tcGFueV9pZCI6IjEifQ.jq0__JqOQJZwAchM7duBwEwh_F-lvEPB_dN349ThHiU',

    // All channels must be entered before to send the API. 
    'levels' => ['EMERGENCY', 'ALERT', 'CRITICAL', 'ERROR', 'WARNING', 'NOTICE', 'INFO', 'DEBUG'],

    // Target channels
    'channels' => ['test-channel'],    
];
