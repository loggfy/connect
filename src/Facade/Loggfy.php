<?php namespace Loggfy\Connect\Facade;

use Illuminate\Support\Facades\Facade;

class Loggfy extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'loggfy';
    }
}
